*** Settings ***
Documentation		Sample Documentation.
...			  ...continues here.

Resource    ../Resources/keywords.robot
Library     OperatingSystem
Library     DateTime
Library     String
Library     Collections
Library     SeleniumLibrary

*** Variables ***
${LIBRARYDIR}
${PROJECTDIR}
${SRCDIR}
${FIREFOXDRIVER}
${BROWSER}    headlessfirefox
${complete_status}    ${NULL}
#&{PLATFORM_CAPABILITIES}    platform=MAC
#headlessfirefox

*** Tasks ***
Sample_Main_Task
    ${project_browser}=    Open Browser To DuckDuckGo Search
    ${search_status}=    Search For Items    search_term=${INITIAL_SEARCH_TERM}
    @{all_search_urls}=    Extract List Of Search Results Urls    project_browser=${project_browser}
    ${complete_status}=    Locate And Navigate To Ferrari Road Cars Wikipedia Page
    @{all_picture_urls}=    Extract List Of Pictures Urls    project_browser=${project_browser}
    Help Print To Console    .....................Printing Search Result Urls Found........................
    Help Print To Console    ................******************START WITH SEARCH RESULTS************************........................
    FOR    ${current_result_url}    IN    @{all_search_urls}
        Help Print To Console    ${current_result_url}
    END
    Help Print To Console    ................*******************END WITH SEARCH RESULTS***********************........................
    Help Print To Console    .....................Printing Pictures Urls Found........................
    Help Print To Console    ................******************START WITH PICTURES************************........................
    FOR    ${current_picture_url}    IN    @{all_picture_urls}
        Help Print To Console    ${current_picture_url}
    END
    Help Print To Console    ................*******************END WITH PICTURES***********************........................
    [Teardown]    Cleanup Tasks To Run After Execution    ${complete_status}


*** Keywords ***

*** Comments ***
