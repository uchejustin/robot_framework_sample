*** Settings ***
Library     ${LIBRARYDIR}/utilities.py
Variables   ${LIBRARYDIR}/project_variables.py


*** Variables ***
${HOMEPAGE_SELECTOR}=     //div[@id="content_homepage"]
${HOMEPAGE_SEARCHBAR_SELECTOR}=     //form[@id="search_form_homepage"]//input[@id="search_form_input_homepage"]
${EXISTING_SEARCH_RESULT_PAGE_SELECTOR}=     //div[@id="header"]
${EXISTING_SEARCHBAR_SELECTOR}=     //form[@id="search_form"]//input[@id="search_form_input"]
${FIRSTPAGE_SEARCH_RESULTS_SELECTOR}=     //div[@id="web_content_wrapper"]//div[@id="links_wrapper"]//div[@id="links"]//div[starts-with(@id, "r1-")]
${SEARCH_RESULTS_BODY_SELECTOR}=     //div[@id="web_content_wrapper"]//div[@id="links_wrapper"]//div[@id="links"]//div[starts-with(@id, "r1-")]//div[starts-with(@class, "result__body")]

${HOMEPAGE_LOCATOR}=     xpath: ${HOMEPAGE_SELECTOR}
${HOMEPAGE_SEARCHBAR_LOCATOR}=    xpath: ${HOMEPAGE_SEARCHBAR_SELECTOR}
${EXISTING_SEARCH_RESULT_PAGE_LOCATOR}=    xpath: ${EXISTING_SEARCH_RESULT_PAGE_SELECTOR}
${EXISTING_SEARCHBAR_SELECTOR}=    xpath: ${EXISTING_SEARCHBAR_SELECTOR}
${FIRSTPAGE_SEARCH_RESULTS_LOCATOR}=    xpath: ${FIRSTPAGE_SEARCH_RESULTS_SELECTOR}
${SEARCH_RESULTS_BODY_LOCATOR}=    xpath: ${SEARCH_RESULTS_BODY_SELECTOR}
${SEARCH_RESULTS_TITLE_LOCATOR}=    xpath: ${SEARCH_RESULTS_TITLE_SELECTOR}
${CAR_PICTURES_URL_LOCATOR}=    xpath: ${CAR_PICTURES_URL_SELECTOR}
${DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}=    xpath: ${DESIRED_WIKIPEDIA_LIST_PAGE}


*** Keywords ***
Open Project Browser
    [Arguments]    ${proj_url}=${BASE_URL}
    Open Browser    url=${proj_url}    browser=${BROWSER}    alias=demo    executable_path=${FIREFOXDRIVER}
    ${demo_browser_obj}=    Get Browser Instance In Use
    Maximise Current Browser Object    ${demo_browser_obj}
    [Return]    ${demo_browser_obj}

Get Project Browser In Use
    ${demo_browser_obj}=    Get Browser Instance In Use
    [Return]    ${demo_browser_obj}

Close Project Browser
    Close Browser

Navigate To Base Or Given Url
    [Arguments]    ${url_to_use}
    Go To    url=${url_to_use}

Verify WIKI Search Identifier Visible
    ${COMPLETE_DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}=    Generate Advanced Search Selector    ${WIKI_SEARCH_IDENTIFIER}
    Wait Until Page Contains Element    ${COMPLETE_DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}
    Wait Until Element Is Visible    ${COMPLETE_DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}

Click Identified WIKI Search Link
    ${COMPLETE_DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}=    Generate Advanced Search Selector    ${WIKI_SEARCH_IDENTIFIER}
    Click Element    ${COMPLETE_DESIRED_WIKIPEDIA_LIST_PAGE_LOCATOR}

Verify Car Pictures Locator Visible
    Wait Until Page Contains Element    ${CAR_PICTURES_URL_LOCATOR}
    Wait Until Element Is Visible    ${CAR_PICTURES_URL_LOCATOR}

Click Car Pictures Locator RightBody
    Click Element    ${CAR_PICTURES_URL_LOCATOR}

Verify Existing Search Bar Locator Visible
    Wait Until Page Contains Element    ${EXISTING_SEARCHBAR_SELECTOR}
    Wait Until Element Is Visible    ${EXISTING_SEARCHBAR_SELECTOR}

Type SearchTerm Into Existing Search Bar Text Field
    [Arguments]    ${search_term}
    Input Text    ${EXISTING_SEARCHBAR_SELECTOR}    ${search_term}    clear=${TRUE}
    Press Keys    ${EXISTING_SEARCHBAR_SELECTOR}    RETURN

Verify Homepage Search Bar Locator Visible
    Wait Until Page Contains Element    ${HOMEPAGE_SEARCHBAR_SELECTOR}
    Wait Until Element Is Visible    ${HOMEPAGE_SEARCHBAR_SELECTOR}

Type SearchTerm Into Homepage Search Bar Text Field
    [Arguments]    ${search_term}
    Input Text    ${HOMEPAGE_SEARCHBAR_SELECTOR}    ${search_term}    clear=${TRUE}
    Press Keys    ${HOMEPAGE_SEARCHBAR_SELECTOR}    RETURN

Check If Search Page Already Open
    Wait Until Page Contains Element    ${EXISTING_SEARCH_RESULT_PAGE_LOCATOR}
    Wait Until Element Is Visible    ${EXISTING_SEARCH_RESULT_PAGE_LOCATOR}


Verify Homepage Opened Succesfully
    Wait Until Page Contains Element    ${HOMEPAGE_LOCATOR}
    Wait Until Element Is Visible    ${HOMEPAGE_LOCATOR}

Verify Search Succesfully Executed
    Wait Until Page Contains Element    ${FIRSTPAGE_SEARCH_RESULTS_SELECTOR}
    Wait Until Element Is Visible    ${FIRSTPAGE_SEARCH_RESULTS_SELECTOR}
