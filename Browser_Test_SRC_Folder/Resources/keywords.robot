*** Settings ***
Resource    ${SRCDIR}/Resources/PageObjects/CommonPO.robot


*** Variables ***
${verify_search_success_status}    ${NULL}
${verify_wikipage_opened_status}    ${NULL}


*** Keywords ***
Repeat Keyword Thrice If Error
    [Arguments]    ${provided_keyword}
    ${date_now}=    Get Current Date    UTC
    &{log_message_dict}=    Create Dictionary    error=${date_now}-->An error occured during execution of keyword-> ${provided_keyword}. Error message->    success=${date_now}-->${provided_keyword} executed successfully.
    @{index_list} =    Create List    1    2    3
    FOR    ${retry_index}    IN    @{index_list}
        ${keyword_execution_status}    ${keyword_execution_output}=    Run Keyword And Ignore Error    ${provided_keyword}
        Run Keyword If    '${keyword_execution_status}' == 'PASS'    Append To File    path=${SRCDIR}/Results/success.log    content=${\n}${log_message_dict}[success]${\n}
        Run Keyword If    '${retry_index}' == '3' and '${keyword_execution_status}' == 'FAIL'    Append To File    path=${SRCDIR}/Results/error.log    content=${\n}${log_message_dict}[error]${keyword_execution_output}${\n}
        Exit For Loop If    '${keyword_execution_status}' == 'PASS'
    END
    [Return]    ${keyword_execution_status}


Cleanup Tasks To Run After Execution
    [Arguments]    ${latest_keyword_status}
    ${date_begining}=    Get Current Date    UTC
    Close All Browsers
    End Success Logging    ${date_begining}
    Run Keyword Unless    '${latest_keyword_status}' == 'PASS'    End Failure Logging    ${date_begining}


End Success Logging
    [Arguments]    ${date_begining}
    Append To File    path=${SRCDIR}/Results/success.log    content=${\n}------------------------------------FINISH ${date_begining}---------------------------------------${\n}


End Failure Logging
    [Arguments]    ${date_begining}
    Append To File    path=${SRCDIR}/Results/error.log    content=${\n}-----------------------------------FINISH ${date_begining}----------------------------------------${\n}
    Fail    msg=An Error Occured During Execution And Has Been Logged


Open Browser To DuckDuckGo Search
    ${project_browser}=    Open Project Browser    proj_url=${BASE_URL}
    [Return]    ${project_browser}


Search For Items
    [Arguments]    ${search_term}
    ${verify_homepage_status}=    Repeat Keyword Thrice If Error    Verify Homepage Opened Succesfully
    ${verify_searchpage_already_open_status}=    Run Keyword Unless    '${verify_homepage_status}' == 'PASS'    Repeat Keyword Thrice If Error    Check If Search Page Already Open
    ${verify__homepage_searchbar_visible_status}=    Run Keyword If    '${verify_homepage_status}' == 'PASS'    Repeat Keyword Thrice If Error    Verify Homepage Search Bar Locator Visible
    ${verify__existing_searchbar_page_status}=    Run Keyword If    '${verify_searchpage_already_open_status}' == 'PASS'    Repeat Keyword Thrice If Error    Verify Existing Search Bar Locator Visible
    
    ${send_homepage_search_status}    ${execution_output}=    Run Keyword And Ignore Error    Type SearchTerm Into Homepage Search Bar Text Field    ${search_term}
    ${send_existing_search_status}    ${execution_output}=    Run Keyword And Ignore Error    Type SearchTerm Into Existing Search Bar Text Field    ${search_term}

    ${verify_search_success_status}=    Run Keyword If    '${send_existing_search_status}' == 'PASS' or '${send_homepage_search_status}' == 'PASS'    Repeat Keyword Thrice If Error    Verify Search Succesfully Executed
    [Return]    ${verify_search_success_status}


Extract List Of Search Results Urls
    [Arguments]    ${project_browser}
    ${project_wait_object}=    Create Custom Webdriver Wait Object    ${project_browser}
    ${search_results_list}=    Get Search Results Url List    ${project_wait_object}    ${project_browser}
    [Return]    ${search_results_list}


Extract List Of Pictures Urls
    [Arguments]    ${project_browser}
    ${project_wait_object}=    Create Custom Webdriver Wait Object    ${project_browser}
    ${urls_of_pictures}=    Get All Pictures Url    ${project_wait_object}    ${project_browser}
    [Return]    ${urls_of_pictures}


Locate And Navigate To Ferrari Road Cars Wikipedia Page
    ${verify_desired_search_item_status}=    Repeat Keyword Thrice If Error    Verify WIKI Search Identifier Visible
    ${navigate_to_wikipedia_page_status}=    Run Keyword If    '${verify_desired_search_item_status}' == 'PASS'    Repeat Keyword Thrice If Error    Click Identified WIKI Search Link
    ${verify_wikipage_opened_status}=    Run Keyword If    '${navigate_to_wikipedia_page_status}' == 'PASS'    Repeat Keyword Thrice If Error    Verify Car Pictures Locator Visible
    [Return]    ${verify_wikipage_opened_status}

