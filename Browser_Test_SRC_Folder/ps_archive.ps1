$SCRIPT_DIR = $PSScriptRoot
$LOGS_PATH = Join-Path $SCRIPT_DIR -ChildPath "Results"
$ARCHIVE_LOGS = Join-Path $SCRIPT_DIR -ChildPath "ArchivedLogs"
$TIME_NOW = (Get-Date -Format "HH_mm_dd_MM_yyyy")
$WRITE_LOG_PATH = Join-Path $ARCHIVE_LOGS -ChildPath "${TIME_NOW}_archive.tgz"
tar -Pczf "${WRITE_LOG_PATH}" "${LOGS_PATH}\*.html" "${LOGS_PATH}\*.htm" "${LOGS_PATH}\*.xml" "${LOGS_PATH}\*.log" "${LOGS_PATH}\*.png"
Get-ChildItem $LOGS_PATH | Where-Object { $_.Name -Match "([a-zA-Z0-9_-]+\.html)|([a-zA-Z0-9_-]+\.htm)|([a-zA-Z0-9_-]+\.xml)|([a-zA-Z0-9_-]+\.log)|([a-zA-Z0-9_-]+\.png)" } | Remove-Item