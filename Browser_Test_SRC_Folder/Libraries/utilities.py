import re
from os.path import join as os_pathjoin
from datetime import datetime

from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from SeleniumLibrary import SeleniumLibrary
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from robot.api.deco import keyword

import project_variables


# Error handling functionalities
@keyword("Log Error Message To File")
def log_error(errorMessage, errorMessageTitle, logsPath, filename="error.log"):
    # helper_print(errorMessage, errorMessageTitle)
    errorFile = os_pathjoin(logsPath, filename)
    with open(errorFile, "a") as f:
        f.write(f"--- {datetime.now().isoformat()} {errorMessageTitle} ---\n")
        f.write(errorMessage)
        f.write("--- End error ---\n\n")


@keyword("Log Success Message To File")
def log_success(successMessage, successMessageTitle, logsPath, filename="success.log"):
    successFile = os_pathjoin(logsPath, filename)
    with open(successFile, "a") as f:
        f.write(f"--- {datetime.now().isoformat()} {successMessageTitle} ---\n")
        f.write(successMessage)
        f.write("\n--- End success ---\n\n")


@keyword("Help Print To Console")
def helper_print(messageBody, title=""):
    logger.console(title, "\n")
    logger.console(messageBody)


# Regex functionalities
@keyword("Regex Search For Pattern")
def search_for_regex_pattern(pattern, text, caseInsensitive=False):
    if caseInsensitive == True:
        searchResultList = list(re.finditer(pattern, text, flags=re.M | re.I))
    else:
        searchResultList = list(re.finditer(pattern, text, flags=re.M))
    if len(searchResultList) == 1:
        return searchResultList[0].groupdict()
    return None


@keyword("Create Custom Web Options")
def create_custom_web_options():
    dev_options = webdriver.FirefoxOptions()
    dev_options.headless = False
    # dev_options.set_capability(name='platform',value='MAC')
    return dev_options


@keyword("Create Custom Browser Object")
def create_custom_browser_object(gekcho_path):
    dev_options = create_custom_web_options()
    browser = webdriver.Firefox(executable_path=gekcho_path, options=dev_options)
    browser.maximize_window()
    return browser


@keyword("Custom Navigate To Url")
def custom_navigate_to_url(browser):
    browser.get(project_variables.BASE_URL)


@keyword("Maximise Current Browser Object")
def maximise_current_browser_object(browser):
    browser.maximize_window()


@keyword("Quit Current Browser Object")
def quit_current_browser_object(browser):
    browser.quit()


@keyword("Create Custom Webdriver Wait Object")
def create_custom_webdriver_wait_object(browser):
    wait_for_el = WebDriverWait(browser, project_variables.TIMEOUT_VAL)
    return wait_for_el


# class CustomSeleniumLibrary(SeleniumLibrary):
# create a new keyword called "get webdriver instance"
#    def get_custom_webdriver_instance(self):
#        return self._current_browser()


@keyword("Get Browser Instance In Use")
def get_browser_instance_in_use():
    selib = BuiltIn().get_library_instance("SeleniumLibrary")
    return selib.driver


@keyword("Get Search Results Url List")
def get_search_results_list(wait_for_el, browser):
    # wait_for_el.until(expected_conditions.visibility_of_element_located((By.XPATH, SEARCH_RESULTS_TITLE_SELECTOR)))
    all_elements_found = browser.find_elements_by_xpath(
        project_variables.SEARCH_RESULTS_TITLE_SELECTOR
    )
    all_files_url = [elem.get_attribute("href") for elem in all_elements_found]
    return all_files_url


@keyword("Get All Pictures Url")
def get_pictures_list(wait_for_el, browser):
    all_elements_found = browser.find_elements_by_xpath(
        project_variables.CAR_PICTURES_URL_SELECTOR
    )
    all_files_url = [elem.get_attribute("href") for elem in all_elements_found]
    return all_files_url


@keyword("Generate Advanced Search Selector")
def generate_advanced_search_selector(search_selection_criteria):
    return project_variables.DESIRED_WIKIPEDIA_LIST_PAGE.format(
        search_identifier=search_selection_criteria
    )
