from os.path import join as os_pathjoin
from os import getcwd as os_getcwd
from os.path import abspath as os_abspath
from os import getenv as os_getenv
import os

# Constants Definitions and Inialization
# WORKING_DIR = os_abspath(os_getcwd())
BASE_URL = "https://duckduckgo.com/"
WORKING_DIR = os_getenv("CURRENTPROJECTDIR")
TIMEOUT_VAL = 6

SEARCH_RESULTS_TITLE_SELECTOR = r'//div[@id="web_content_wrapper"]//div[@id="links_wrapper"]//div[@id="links"]//div[starts-with(@id, "r1-")]//div[starts-with(@class, "result__body")]//h2[starts-with(@class, "result__title")]//a[starts-with(@class, "result__a") and starts-with(@href, "https:")]'
CAR_PICTURES_URL_SELECTOR = r'//div[@id="content"]//div[@id="bodyContent"]//div[starts-with(@class, "thumb")]//a[@class="image" and contains(@href, ".jpg")]'
DESIRED_WIKIPEDIA_LIST_PAGE = '//div[@id="web_content_wrapper"]//div[@id="links_wrapper"]//div[@id="links"]//div[starts-with(@id, "r1-")]//div[starts-with(@class, "result__body")]//h2[starts-with(@class, "result__title")]//a[starts-with(@class, "result__a") and contains(translate(@href, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"), "{search_identifier}")]'
INITIAL_SEARCH_TERM = "ferrari cars wikipedia"
WIKI_SEARCH_IDENTIFIER = "list_of_ferrari_road_cars"
