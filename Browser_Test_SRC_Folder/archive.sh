SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
LOGS_PATH="${SCRIPT_DIR}/Results/"
ARCHIVE_LOGS="${SCRIPT_DIR}/ArchivedLogs/"
TIME_NOW="$(date +'%H_%M_%S__%d_%m_%Y__')"
ARCHIVE_FILE="${ARCHIVE_LOGS}${TIME_NOW}_archive.tgz"
tar -Pczf "${ARCHIVE_FILE}" $LOGS_PATH*.html $LOGS_PATH*.xml $LOGS_PATH*.log $LOGS_PATH*.png
rm $LOGS_PATH*.html $LOGS_PATH*.xml $LOGS_PATH*.log $LOGS_PATH*.png
