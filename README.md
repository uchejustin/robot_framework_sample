# robot_framework_sample
Sample robot framework project for parsing excel files into pandas dataframe tables
## How to get the project
1. `git clone git@gitlab.com:uchejustin/robot_framework_sample.git`

## How to run the project
### Running using pipenv
1. Install Python
2. Install pipenv `pip install pipenv`
3. Install the dependencies `pipenv install` while in the project work directory containing the Pipfile
4. Create an `.env` file. Project already have `.env.template` file you can copy the values from there
to your `.env` file
5. Execute `pipenv run robot -d ${SCRIPT_DIR}/Project_SRC_Folder/Results --timestampoutputs --loglevel debug --reporttitle "Sample Excel Processing RPA Summary Report" --logtitle "Sample Excel Processing RPA Logs" -v LIBRARYDIR:${PROJECT_LIBRARYDIR} -v PROJECTDIR:${PROJECT_WORKDIR} -t Sample_Task_Name_To_Run ${SCRIPT_DIR}/Project_SRC_Folder/Tasks/sample_rpa.robot`.
The working directory path should replace the SCRIPT_DIR, and the path to the project data folder should replace the PROJECT_WORKDIR variable. Also the path to the python user-defined libraries should replace the PROJECT_LIBRARYDIR variable.
OR alternatively run from the work directory containing the Pipfile.

### Running with a shell script (preferred option)
1. Install Python
2. Install pipenv `pip install pipenv`
3. Install the dependencies `pipenv install` while in the project work directory containing the Pipfile
4. Execute bash script `sh sh_entrypoint.sh`


### Running with a powershell script (preferred option on windows)
1. Install Python
2. Install pipenv `pip install pipenv`
3. Install the dependencies `pipenv install` while in the project work directory containing the Pipfile
4. Execute powershell script `.\entrypoint.ps1`


### Command Line Options
The command line syntax is `pipenv run robot [options] robot_file_to_execute` or just `robot [options] robot_file_to_execute` if installed and running locally without using virtual environments. The command line options include:
1. --timestampoutputs  specying that timestamps should be appended to report/log files to avoid overwriting with each run  
2. --loglevel  specifying desired log level
3. --reporttitle  specifying the name to be show on the html report file
4. --logtitle  specifying the name to be show on the html log file
5. --v  specifying any global variable to be set from command line
6. --t  specifying a particular test/task to execute