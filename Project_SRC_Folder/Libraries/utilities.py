import re
from os.path import join as os_pathjoin
from datetime import datetime
from glob import glob

from smtplib import SMTP
from robot.api.deco import keyword
from robot.api import logger
import openpyxl
import pandas as pd

import error_classes, excel_variables

@keyword("Print Hello World")
def print_hello():
    logger.console("hello world from rpa")


# Confirm File Existence Preconditions and get File Path
@keyword("Get Single Excel Sheet Path")
def get_single_excel_template():
    types = ("*.xls", "*.xlsb", "*.xlsm", "*.xlsx")  # the tuple of file types
    searchResult = []
    for fileType in types:
        searchPattern = os_pathjoin(excel_variables.PATH_TO_EXCEL, fileType)
        searchResult.extend(glob(searchPattern))
    if len(searchResult) > 1:
        raise error_classes.MultipleExcelTemplateError
    if len(searchResult) < 1:
        raise error_classes.MissingExcelTemplateError
    return searchResult[0]


@keyword("Check Excel Sheet Not Empty")
def check_if_excelsheet_isnot_empty(worksheet):
    return worksheet.max_column > 1 and worksheet.max_row > 1


# Reading Excel Sheet
@keyword("Read Excel Sheet")
def read_excel_sheet(workbook, sheetIndex=0):
    if len(workbook.sheetnames) <= sheetIndex:
        raise error_classes.ExcelSheetDoesNotExist
    sheetName = workbook.sheetnames[sheetIndex]
    worksheet = workbook[sheetName]
    return worksheet


# Reading Excel Workbook
@keyword("Load Excel Workbook")
def read_excel_workbook(excelFilePath):
    return openpyxl.load_workbook(filename=excelFilePath)


# Converting Excel Sheet to Dataframe
@keyword("Convert Excel Sheet To Dataframe")
def convert_excelsheet_to_dataframe(filePath, sheetName=0):
    return pd.read_excel(filePath, engine="openpyxl",  sheet_name=sheetName)


# Log handling functionalities
@keyword("Log Message To File")
def log_message(message, messageTitle, logsPath, filename):
    logFile = os_pathjoin(logsPath, filename)
    with open(logFile, "a") as f:
        f.write(f"--- {datetime.now().isoformat()} {messageTitle} ---\n")
        f.write(message)
        f.write("\n--- End error ---\n\n")


@keyword("Help Print To Console")
def helper_print(messageBody, title=""):
    logger.console(title, "\n")
    logger.console(messageBody)


@keyword("Send Email")
def send_email(body, subject):
    with SMTP(excel_variables.EMAIL_CONNECTION_DETAILS) as dmSMTP:
        dmSMTP.ehlo()
        dmSMTP.connect(host=excel_variables.EMAIL_CONNECTION_DETAILS)
        emailSubject = subject
        emailBody = body
        msg = f'Subject: {emailSubject}\n\n{emailBody}'
        dmSMTP.sendmail(excel_variables.EMAIL_SENDER_ADDRESS, excel_variables.DEV_EMAIL_RECIPIENTS, msg)
        dmSMTP.quit()


# Regex functionalities
@keyword("Regex Search For Pattern")
def search_for_regex_pattern(pattern, text, caseInsensitive=False):
    if caseInsensitive == True:
        searchResultList = list(re.finditer(pattern, text, flags=re.M | re.I))
    else:
        searchResultList = list(re.finditer(pattern, text, flags=re.M))
    if len(searchResultList) > 0:
        return [match[0] for match in searchResultList] #searchResultList
    return None


@keyword("Convert Regex Result To Dataframe")
def convert_regex_results_to_dataframe(regexResult):
    if regexResult is not None:
        groupList = [match.groupdict() for match in regexResult]
        df = pd.DataFrame(groupList)
        return df
    return None


@keyword("Normalize Raw Text")
def normalize_rawtext(text):
    return text.replace("\n", " ").upper()


@keyword("Remove Empty Spaces")
def remove_empty_spaces(text):
    return text.replace(" ", "")
