import sys
from os.path import join as os_pathjoin
from datetime import datetime
from glob import glob

import pandas as pd
from robot.api.deco import keyword

import excel_variables, utilities


@keyword("Trigger Error Handling Process")
def handle_errors(errorMessage, logsPath, log=False, report=False):
    errorMessageTitle = "Technical Error with processing of excel file (Robot Process)"
    utilities.helper_print(errorMessage)
    if log:
        utilities.log_message(errorMessage, errorMessageTitle, logsPath, "error.log")
    if report:
        utilities.send_email(errorMessage, errorMessageTitle)


# Log completion status
@keyword("Trigger Successful Process Reporting")
def handle_success_logs(
    logsPath,
    log=False,
    report=False,
):
    successMessageBody = "Processing of excel file has been completed succesfully (Robot Process) "
    successMessageTitle = "Sample Success Title"
    utilities.helper_print(successMessageBody)
    if log:
        utilities.log_message(successMessageBody, successMessageTitle, logsPath, "success.log")
    if report:
        utilities.send_email(successMessageBody, successMessageTitle)


@keyword("Manage Excel Conversion Process")
def start_process():
    excelFilePath = utilities.get_single_excel_template()
    excelDF = utilities.convert_excelsheet_to_dataframe(excelFilePath)
    utilities.helper_print(excelDF.head())

    handle_success_logs(
        excel_variables.LOG_PATH,
        log=False,
        report=False,
    )
    return excelDF.to_string()

#Entry point of file
@keyword("Start Excel Conversion")
def main():
    try:
        return start_process()
    except Exception:
        import traceback
        err = "".join(traceback.format_exception(*sys.exc_info()))
        handle_errors(err, excel_variables.LOG_PATH, log=False, report=False)
        raise