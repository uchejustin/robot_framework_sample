#User Defined Error Classes
class MissingExcelTemplateError(Exception):
    pass

class MultipleExcelTemplateError(Exception):
    pass

class ExcelFirstSheetEmpty(Exception):
    pass

class ExcelSheetDoesNotExist(Exception):
    pass