from os.path import join as os_pathjoin
from os import getenv as os_getenv
import os

# Constants Definitions and Inialization
EMAIL_CONNECTION_DETAILS = f'{os_getenv("EMAIL_SERVER")}:{os_getenv("EMAIL_PORT")}'
WORKING_DIR = os_getenv("RPAPROJECTDIR")
PATH_TO_PDF = os_pathjoin(WORKING_DIR, "SourceData")
LOG_PATH = os_pathjoin(WORKING_DIR, "Logs")
PATH_TO_EXCEL = os_pathjoin(WORKING_DIR, "ExcelTemplate")
EMAIL_SENDER_ADDRESS = os_getenv("NO_REPLY_EMAIL")
DEV_EMAIL_RECIPIENTS = [email for email in os_getenv("DEV_EMAIL_RECIPIENTS").split(" ")]

PHONE_NUMBER_PATTERN =  r"\d{3} \d{7}"

