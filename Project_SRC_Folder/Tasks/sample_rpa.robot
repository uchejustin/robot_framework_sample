*** Settings ***
Documentation		Sample Documentation.
...			  ...continues here.

Resource    ../Resources/keywords.robot
Library     OperatingSystem

*** Variables ***
${PROJECTDIR}    ${EXECDIR}
${LIBRARYDIR}    ${SPACE}
${SAMPLE_MESSAGE}    Hello, ${SPACE * 3}world!


*** Tasks ***
Sample Task Name To Run
    Log to console    ${SPACE}
    Execute Excel Conversion Process


Second Sample Print Task
    Log to console    ${SPACE}
    Log to console    ${SAMPLE_MESSAGE}
    Log to console    Exec:${SPACE}${\n}${EXECDIR}
    Log to console    Curr:${SPACE}${\n}${CURDIR}
    Log to console    Temp:${SPACE}${\n}${TEMPDIR}${\n}
    Process greeting for today

Copy Test File Task
    Log to console         ${SPACE}
#    Create Directory      ${DEST_DIR}
#    Copy File             ${SOURCE_DIR}test.xlsx    ${DEST_DIR}stuff.txt
    No Operation

Normal test case with embedded arguments
    Log to console       ${SPACE}
    ${firstanswer} =     Convert To Integer    2
    ${secondanswer} =    Convert To Integer    3
    The result of 1 + 1 should be ${firstanswer}
    The result of 1 + 2 should be ${secondanswer}

For Loop Test
    Log to console      ${SPACE}
    &{TESTLIST} =       Create Dictionary       name=Robot  id=2
    &{MY_DICT} =        Create Dictionary       sample=&{TESTLIST}
    Log to console      ${\n}${MY_DICT}
    Log to console      ${\n}${TESTLIST}
    Log to console      ${\n}${MY_DICT}[sample][name]${\n}
    @{mylist} =         Create List     item_1      item_2      item_3
    FOR    ${word}    IN    @{mylist}
        Log to console    ${\n}${word}
    END

*** Keywords ***
The result of ${calculation} should be ${expected}
    ${result} =        Evaluate    ${calculation}
    Should Be Equal    ${result}    ${expected}

*** Comments ***