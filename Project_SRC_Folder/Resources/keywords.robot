*** Settings ***
Library     ${LIBRARYDIR}/utilities.py
Library     ${LIBRARYDIR}/excel_rpa_main.py
Variables   ${LIBRARYDIR}/excel_variables.py

*** Keywords ***
Execute Excel Conversion Process
    ${dataframe_string_output} =    Start Excel Conversion
    ${normalized_text} =    Normalize Raw Text       ${dataframe_string_output}
    @{phone_numbers} =    Regex Search For Pattern       ${PHONE_NUMBER_PATTERN}    ${normalized_text}
    FOR    ${number}    IN    @{phone_numbers}
        Help Print To Console    ${number}
    END