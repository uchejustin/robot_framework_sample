# Executes robot file with pipenv and enviroment and optionally archives"output" directory.
$SCRIPT_DIR = $PSScriptRoot
$PROJECT_RPA_SRCDIR = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder"
$PROJECT_RPA_WORKDIR = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder" | Join-Path -ChildPath "Project_Folder"
$PROJECT_RPA_LIBRARYDIR = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder" | Join-Path -ChildPath "Libraries"
$FIREFOX_DRIVER_PATH = Join-Path $PROJECT_RPA_WORKDIR -ChildPath "geckodriver_v0_28_0_win64" | Join-Path -ChildPath "geckodriver.exe"
$PIPENVPATH = Join-Path $SCRIPT_DIR -ChildPath "Pipfile"
[Environment]::SetEnvironmentVariable("PIPENV_PIPFILE", $PIPENVPATH, 'Process')
[Environment]::SetEnvironmentVariable("CURRENTPROJECTDIR", $PROJECT_RPA_WORKDIR, 'Process')
$RESULTPATH = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder" | Join-Path -ChildPath "Results"
$ROBOTPATH = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder" | Join-Path -ChildPath "Tasks" | Join-Path -ChildPath "entrypoint.robot"
py -3.8 -m pipenv run robot -d $RESULTPATH --timestampoutputs --loglevel debug --reporttitle "Browser Test RPA Summary Report" --logtitle "Browser Test RPA Logs" -v FIREFOXDRIVER:${FIREFOX_DRIVER_PATH} -v LIBRARYDIR:${PROJECT_RPA_LIBRARYDIR} -v SRCDIR:${PROJECT_RPA_SRCDIR} -v PROJECTDIR:${PROJECT_RPA_WORKDIR} -t Sample_Main_Task $ROBOTPATH

$DAY_NOS = (Get-Date -Format "dd")
if ("$DAY_NOS" -eq "01" -or "$DAY_NOS" -eq "15") {
    $ARCHIVE_SCRIPT_PATH = Join-Path $SCRIPT_DIR -ChildPath "Browser_Test_SRC_Folder" | Join-Path -ChildPath "ps_archive.ps1"
    &${ARCHIVE_SCRIPT_PATH}
}