#!/bin/bash -ex

# Executes robot file with pipenv and enviroment and optionally archives"output" directory.
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
FIREFOX_DRIVER_PATH=/Users/administrator/webdriversbin/geckodriver
PROJECT_RPA_SRCDIR=$SCRIPT_DIR/Browser_Test_SRC_Folder
PROJECT_RPA_WORKDIR=$SCRIPT_DIR/Browser_Test_SRC_Folder/Project_Folder
PROJECT_RPA_LIBRARYDIR=${SCRIPT_DIR}/Browser_Test_SRC_Folder/Libraries
export PIPENV_PIPFILE=${SCRIPT_DIR}/Pipfile
export CURRENTPROJECTDIR=${PROJECT_RPA_WORKDIR} 
# export PYTHONPATH=${PROJECT_RPA_LIBRARYDIR}
pipenv run robot -d ${SCRIPT_DIR}/Browser_Test_SRC_Folder/Results --timestampoutputs --loglevel debug --reporttitle "BrowserTest RPA Summary Report" --logtitle "BrowserTest RPA Logs" -v FIREFOXDRIVER:${FIREFOX_DRIVER_PATH} -v LIBRARYDIR:${PROJECT_RPA_LIBRARYDIR} -v PROJECTDIR:${PROJECT_RPA_WORKDIR} -v SRCDIR:${PROJECT_RPA_SRCDIR} -t Sample_Main_Task ${SCRIPT_DIR}/Browser_Test_SRC_Folder/Tasks/entrypoint.robot
DAY_NOS="$(date +'%d')"
if [[ "$DAY_NOS" == "01" || "$DAY_NOS" == "15" ]]; then
    sh ${SCRIPT_DIR}/Browser_Test_SRC_Folder/archive.sh
else
    :
fi