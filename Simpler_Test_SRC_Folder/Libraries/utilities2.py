import re
from robot.api.deco import keyword
from robot.api import logger


@keyword("Print Hello World")
def print_hello():
    logger.console("hello world from rpa")


@keyword("Help Print To Console")
def helper_print(messageBody, title=""):
    logger.console(title, "\n")
    logger.console(messageBody)


@keyword("Normalize Raw Text")
def normalize_rawtext(text):
    return text.replace("\n", " ").upper()


# Regex functionalities
@keyword("Regex Search For Pattern")
def search_for_regex_pattern(pattern, text, caseInsensitive=False):
    if caseInsensitive == True:
        searchResultList = list(re.finditer(pattern, text, flags=re.M | re.I))
    else:
        searchResultList = list(re.finditer(pattern, text, flags=re.M))
    if len(searchResultList) > 0:
        return [match[0] for match in searchResultList]  # searchResultList
    return None
